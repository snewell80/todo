// public/core.js
var todoApp = angular.module('todoApp', []);

function mainController($scope, $http) {
  $scope.formData = {};

  //when the page loads get all todos from db
  $http.get('/api/todos')
    .success(function(data){
      $scope.todos = data;
      console.log(data);
    })
    .error(function (data){
      console.log('Error: ' + data);
    });

    //submit form data to node api
    $scope.createTodo = function() {
      $http.post('/api/todos', $scope.formData)
        .success(function (data) {
          $scope.formData = {}; //clear form on successful creation
          $scope.todos = data;
          console.log(data);
        })
        .error(function(data) {
          console.log('Error: ' + data);
        });
    };

    //delete todo 
    $scope.deleteTodo = function(id) {
      $http.delete('/api/todos/' + id)
        .success(function(data) {
          $scope.todos = data;
          console.log(data);
        })
        .error(function(data) {
          console.log('Error: ' + data);
        });
    };
}