// server.js

// set up ===============
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var morgan = require('morgan');
var bodyparser = require('body-parser');
var methodOverride = require('method-override');
//var config = require('config.json')//encapsulate configuration into single file

// config ==============
mongoose.connect('mongodb://127.0.0.1:27017/tododb')

app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyparser.urlencoded({'extended': 'true'}));
app.use(bodyparser.json());
app.use(bodyparser.json({type: 'application/vnd.api+jaon'}));
app.use(methodOverride());

// model ================
// this should be moved to its own file prior to integration
//with the digital home applciation
var Todo = mongoose.model('Todo', {
  text : String
});

//routes ==============
//api ----------------
//////these can also be moved to their own files
//get all todos
app.get('/api/todos', function (req, res){
  Todo.find(function(err, todos){
    if(err)
      res.send(err);
    console.log('todos: ' + todos)
    res.json(todos);//return all todos in json format
  });
});

//create a new todo and send back updated todo list
app.post('/api/todos', function(req, res){
  //create todo using data form ajax angular request
  Todo.create({
    text : req.body.text,
    done : false
  }, function (err, todo) {
    if (err)
      res.send(err);
    //get all todos including the new one and return to app
    Todo.find(function (err, todos) {
      if (err)
        res.send(err);
      res.json(todos);
    });
  });
});

//delete a todo
app.delete('/api/todos/:todo_id', function(req, res){
  Todo.remove({
    _id : req.params.todo_id
  }, function(err, todo) {
    if (err)
      res.send(err);
    //return all todos after updating the remove call
    Todo.find(function(err, todos) {
      if (err)
        res.send(err);
      res.json(todos);
    });
  });
});

//application routes (angular front end routes)----------
//////these can be moved to their own files
app.get('*', function(req, res){
  res.sendfile('./public/index.html');//loads single page file for the app
});


// start application =============================
app.listen(12345);
console.log('Application: Todo, listening on port 12345');